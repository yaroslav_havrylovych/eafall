# EAFall 

The game project started at the mid of 2013.
Starting from the April 2015 has at least one the fully dedicated developer till 2016.
Huge pause was till August 2016. In progress again. Released 03.03.2017.
Take a look on the repo usage to get updated info.

Project wiki you can find [here](https://bitbucket.org/yaroslav_havrylovych/eafall/wiki/Home).

Play market page is [here](https://play.google.com/store/apps/details?id=com.yaroslavlancelot.eafall).

*Note* :
The project uses [AndEngine](https://github.com/nicolasgramlich/AndEngine) graphics, physics and network engines.


Everything is under [Apache version 2 license](license.txt)


---------------------------------------
Yaroslav Havrylovych